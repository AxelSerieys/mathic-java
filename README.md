# Mathic - Compilateur de notation mathématique
Axel Serieys

## Compilation de l'application
1. En utilisant le script fourni (linux seulement) : 
`./compile.sh`
2. En exécutant la commande suivante dans une invite de commande (Maven) :
`mvn clean package`
   
## Exécution des tests unitaires
1. En mode normal :\
`mvn clean test`
   

2. En mode silencieux\
`mvn -Dquiet.mode clean test`\
L'application se lance en mode silencieux (le logging n'est pas effectué)
   
Les tests unitaires sont exécutés par l'intégration continue :\
https://gitlab.com/AxelSerieys/mathic-java/-/pipelines

## Utilisation de l'application
Il y a trois façon de l'utiliser :
1. Exécution d'un calcul : `java -jar mathic.jar -c "<calcul>"`
2. Compilation d'un fichier `java -jar mathic.jar -f <fichier>`
3. Création d'un serveur TCP : `java -jar mathic.jar -s <Nom/Adresse hôte>:<Port>` (localhost:12345 démarre un serveur à l'adresse localhost et sur le port TCP 12345)
4. Utilisation à travers l'IHM : `java -jar mathic.jar`
Avec l'IHM, il est possible de choisir d'exécuter un calcul avec le compilateur local ou d'envoyer une requête à un serveur mathic
   
L'option `-q` permet de lancer l'application en mode 'silencieux'. Cela signifie que le logging ne sera pas effectué.

## Fonctionnalités du compilateur :
* Opérations algébriques de base (`+`, `-`, `*`, `\`)
* Parenthèses pour les priorités de calcul
* Opérateurs de comparaison (`<`,` >`, `<=`, `>=`, `==`)
* Opérateur modulo (`%`)
* Commentaires (`#`)
* Constantes (`%pi%`)

## Information sur les booléens
Un booléen peut être obtenu en réalisant une comparaison (ex: 1 > 2 renvoie true alors que 2 >= 1 renvoie false).\
Lorsque true ou false se trouve dans une opération mathématique, ils sont respectivement remplacés par 0 et 1.\
`1 + (2 > 3)` renvoie 1, alors que\
`1 + (2 < 3)` renvoie 2.