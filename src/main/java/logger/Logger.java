package logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import application.Configuration;
import application.Popup;
import compilateur.Resultat;
import exception.ConfigurationException;
import exception.LoggerException;

/**
 * Classe Logger permetant de gérer les fichiers de log de l'application
 * @author Axel Serieys
 *
 */
public class Logger {

    private String offset = "";
    private File file;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");
    private Configuration config = Configuration.getInstance();

    /**
     * Constructeur de la classe
     * @param fileName, le nom du fichier qui contiendra les logs
     * @throws LoggerException si une erreur se produit lors de la création du fichier
     */
    public Logger(String fileName) throws LoggerException {
        if(!Configuration.QUIET_MODE) {
            try {
                this.file = new File(new File(config.get("logs.repertoire")), fileName);
                file.createNewFile(); // Création du fichier s'il n'existe pas
                log_write("LOG", "DÉBUT DE LA COMPILATION (" + dateFormat.format(new Date()) + ")", false);
            } catch (IOException e) {
                throw new LoggerException("Erreur : le fichier de log ne peut pas être créé : ", file.getName());
            } catch (ConfigurationException e) {
                new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
            }
        }
    }

    /**
     * Constructeur de la classe
     * @param fileName, le nom du fichier qui contiendra les logs
     * @param offset, contient un certain nombre d'espace (tabulations) qui préfixeront toutes les lignes de du fichier
     * @throws LoggerException si une erreur se produit lors de la création du fichier
     */
    public Logger(String fileName, String offset) throws LoggerException {
        this(fileName);
        this.offset = offset;
    }

    /**
     * Affiche un message de debug dans les logs
     * @param message, le message à afficher
     * @throws LoggerException, si une erreur se produit durant l'écriture du message dans le fichier
     */
    public void debug(String message) throws LoggerException {
        log_write("DEBUG", message);
    }

    /**
     * Affiche un message de log dans les logs
     * @param message, le message à afficher
     * @throws LoggerException, si une erreur se produit durant l'écriture du message dans le fichier
     */
    public void log(String message) throws LoggerException {
        log_write("LOG", message);
    }

    /**
     * Affiche le calcul donné dans les logs
     * @param message, le calcul
     * @throws LoggerException, si une erreur se produit durant l'écriture du calcul dans le fichier
     */
    public void calcul(String message) throws LoggerException {
        log_write("CALCUL", message);
    }

    /**
     * Affiche un message d'erreur dans les logs
     * @param message, le message d'erreur
     * @throws LoggerException, si une erreur se produit durant l'écriture du message dans le fichier
     */
    public void erreur(String message) throws LoggerException {
        log_write("ERREUR", message);
        log_write("FIN", "==========");
    }
    
    /**
     * Affiche un message d'erreur dans les logs
     * @param message, le message d'erreur
     * @param ligne, la ligne où elle s'est produite
     * @throws LoggerException, si une erreur se produit durant l'écriture de l'erreur dans le fichier
     */
    public void erreur(String message, int ligne) throws LoggerException {
        log_write("ERREUR", message + " À LA LIGNE " + ligne);
        log_write("FIN", "==========");
    }

    /**
     * Affiche un message d'erreur dans les logs
     * @param message, le message d'erreur
     * @param ligne, la ligne où elle s'est produite
     * @param colonne, la colonne où elle s'est produite
     * @throws LoggerException, si une erreur se produit durant l'écriture de l'erreur dans le fichier
     */
    public void erreur(String message, int ligne, int colonne) throws LoggerException {
        log_write("ERREUR", message + " À LA LIGNE " + ligne + ", COLONNE " + colonne);
        log_write("FIN", "==========");
    }

    /**
     * Affiche le résultat de la compilation dans les logs
     * @param resultat, le résulat
     * @param fin, true si la compilation se termine. Dans ce cas-là, on affiche la date de fin.
     * @throws LoggerException, si une erreur se produit durant l'écriture du résultat dans le fichier
     */
    public void resultat(String resultat, boolean fin) throws LoggerException {
        log_write("RESULTAT", resultat);
        if(fin)
            fin();
    }

    public void resultat(Resultat res, boolean fin) throws LoggerException {
        resultat((String) res.getValeur(), fin);
    }

    public void fin() throws LoggerException {
        log_write("FIN", "========== ("+ dateFormat.format(new Date()) + ")");
    }

    /**
     * Affiche un calcul donné dans les logs
     * @param string, l'entrée
     * @throws LoggerException, si une erreur se produit durant l'écriture de l'entrée dans le fichier
     */
    public void entree(String string) throws LoggerException {
        log_write("ENTRÉE", string);
    }

    public void http(int responseCode, String responseMessage) throws LoggerException {
        log_write("HTTP", responseCode + " " + responseMessage);
    }

    /**
     * Affiche la RPN (notation polonaise inversée) dans les logs
     * @param rpn, la RPN à afficher sous forme de tableau
     * @throws LoggerException, si une erreur se produit durant l'écriture de la RPN dans le fichier
     */
    public void RPN(String[] rpn) throws LoggerException {
        StringBuilder toPrint = new StringBuilder();
        for(String s : rpn) {
            toPrint.append(s).append(" ");
        }
        log_write("RPN", toPrint.toString());
    }

    /**
     * Affiche une ligne vide (séparateur) dans le fichier
     * @throws LoggerException, si une erreur se produit durant l'écriture de la ligne dans le fichier
     */
    private void print_empty_line() throws LoggerException {
        try {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(this.file, true))) {
                writer.append("\n");
            }
        } catch(IOException e) {
            throw new LoggerException("Erreur : Impossible d'écrire dans le fichier de log. Veuillez contacter un administrateur", file.getName());
        }
    }

    /**
     * Fonction wrapper permettant d'écrire une ligne dans le fichier
     * @param title, le titre du message (sera affiché au début de la ligne entre [])
     * @param message, le message à afficher
     * @param append, optionnel. Si rien n'est passé ou si true est passé, le message sera ajouté à la suite du fichier, sinon le fichier sera vidé et la ligne sera affichée à la première ligne.
     * @throws LoggerException, si une erreur se produit durant l'écriture de la ligne dans le fichier
     */
    private void log_write(String title, String message, boolean... append) throws LoggerException {
        if(Configuration.QUIET_MODE)
            return;

        String toPrint = this.offset + "[" + title + "] : " + message + "\n";
        boolean apd = (append.length  == 0 || append[0]);

        try {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(this.file, apd))) {
                writer.append(toPrint);
            }
        } catch(IOException e) {
            throw new LoggerException("Erreur : Impossible d'écrire dans le fichier de log. Veuillez contacter un administrateur", file.getName());
        }
    }
}
