import http.UndertowServer;
import logger.Logger;
import exception.CompileException;
import exception.ConfigurationException;
import exception.LoggerException;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import com.diogonunes.jcolor.Attribute;
import static com.diogonunes.jcolor.Ansi.colorize;

import application.Configuration;
import application.IHM;
import application.Popup;
import compilateur.Compilateur;
import compilateur.reponse.Reponse;

@Command(name = "Mathic", mixinStandardHelpOptions = true,
        description = "Compilateur mathématique", version = "1.0")
/**
 * Point d'entrée de l'application. Récupère les éventuelles options de lancement, et démarre l'application
 * dans le mode demandé
 * L'option -f permet de compiler un fichier par ligne de commande
 * L'option -c permet de compiler un calcul donné par ligne de commande
 * L'option -s permet de démarrer le serveur mathic
 * Aucune option affiche l'IHM
 * 
 * La biliothèque picocli est utilisée pour la gestion des options
 * 
 * @author Axel Serieys
 *
 */
public class Main implements Callable<Integer> {

    @Option(names = {"-f", "fichier"}, description = "Le fichier qui sera compilé") private File p_file;
    @Option(names = {"-c", "calcul"}, description = "Un calcul qui sera exécuté") private String p_calcul;
    @Option(names = {"-s", "serveur"}, description = "Démarre un serveur") private String p_host;
    @Option(names = {"-q", "quiet"}, description = "Mode silencieux. Rien n'est loggé") private boolean p_quiet;

    private Logger logger;
    private Compilateur compilateur;

    /**
     * Fonction appelée par picocli pour gérer les options passées en paramètre
     */
    public Integer call() {
        try {
            Configuration conf = Configuration.newInstance();
            Configuration.QUIET_MODE = p_quiet;

            if (p_file != null) {
                logger = new Logger(conf.get("logs.fichier_client"));
                compilateur = new Compilateur(logger);
                lireFichier(p_file);
            } else if (p_calcul != null) {
                logger = new Logger(conf.get("logs.fichier_client"));
                compilateur = new Compilateur(logger);
                executerCommande(p_calcul);
            } else if(p_host != null) {
				new UndertowServer(p_host).start();
        	} else {
                ouvrirInterface();
            }
        } catch(LoggerException | ConfigurationException e) {
        	new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
        }
        return 0;
    }

    /**
     * Compilation d'un fichier donné
     * @param fichier (File), le fichier à compiler
     */
    public void lireFichier(File fichier) {
    	try {
			if(fichier != null) {
				FileReader fr = new FileReader(fichier);
				BufferedReader br = new BufferedReader(fr);
				String ligne;
				List<String> calculs = new ArrayList<String>(5);
				
				while((ligne = br.readLine()) != null) {
					calculs.add(ligne);
				}
				fr.close();
				
				try {
					Reponse[] reponses = compilateur.parse(calculs.toArray(new String[0]));
					System.out.println(Reponse.arrayToString(reponses));
				} catch(LoggerException | CompileException | ConfigurationException e) {
					e.printStackTrace();
				}
			} else {
	        	new Popup(Popup.T_ERREUR, "Fichier introuvable", "Fichier " + fichier.getPath() + " introuvable.").show();
				System.exit(1);
			}
    	} catch(IOException e) {
        	new Popup(Popup.T_ERREUR, "IOException", e.getMessage()).show();
    	}
    }

    /**
     * Exécution d'un simple calcul par ligne de commande
     * @param commande (String), le calcul à compiler
     * @return Reponse[] les réponses de chaque calcul
     */
    public Reponse[] executerCommande(String commande) {
        try {
            try {
            	String[] cmd = {commande};
            	Reponse[] reponses = compilateur.parse(cmd);
                System.out.println(Reponse.arrayToString(reponses));
                return reponses;
            } catch (CompileException e) {
                System.out.println(colorize("ERREUR : "+ e.getMessage(), Attribute.RED_TEXT()));
                if(Configuration.QUIET_MODE)
                    System.exit(2);
                if (e.getLine() != null) {
                    if (e.getColonne() != null) {
                        logger.erreur(e.getMessage(), e.getLine(), e.getColonne());
                    } else {
                        logger.erreur(e.getMessage(), e.getLine());
                    }
                } else {
                    logger.erreur(e.getMessage());
                }
            }
        } catch(LoggerException | ConfigurationException e) {
        	new Popup(Popup.T_ERREUR, "Exception", e.getMessage()).show();
        }
        return null;
    }

    /**
     * Affiche l'IHM.
     */
    public void ouvrirInterface() {
    	IHM.main(null);
    }

    /**
     * Point d'entrée (fonction main) du programme
     * @param args
     */
    public static void main(String[] args) {
        System.exit(new CommandLine(new Main()).execute(args));
    }

}
