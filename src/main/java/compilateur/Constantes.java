package compilateur;

import application.Configuration;
import application.Popup;
import exception.ConfigurationException;

/**
 * Classe abstraite permettant de gérer les constantes mathématiques (ex: PI).
 * Les constantes sont accessibles grâce à un attribut public, ou grâce à un fonction du même nom
 * qui retournera la même valeur pour une précision choisie
 * @author Axel Serieys
 *
 */
public abstract class Constantes {

	public static final double PI = Math.PI;
	private static final Configuration config = Configuration.getInstance();
	
	// CODES DE RETOUR
	public static final int ERREUR_CONFIGURATION_INVALIDE = 0x1;
	public static final int ERREUR_CONFIGURATION_INTROUVABLE = 0x2;
	public static final int ERREUR_IOEXCEPTION = 0x3;
	
	/**
	 * Retourne la valeur de PI pour une précision donnée
	 * @return double, la valeur de PI
	 */
	public static double PI() {
		try {
			return Double.parseDouble(String.format("%." + config.getInt("compilateur.precision") + "f", PI).replaceAll(",", "."));
		} catch (NumberFormatException e) {
			new Popup(Popup.T_ERREUR, "Erreur", e.getMessage()).show();
		} catch (ConfigurationException e) {
			new Popup(Popup.T_ERREUR, "Configuration incorrecte", e.getMessage()).show();
		}
		return 0.0;
	}
}
