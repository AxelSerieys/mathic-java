package compilateur;

import exception.CompileException;

/**
 * Permet de réaliser des calculs simples sur les deux valeurs données et pour un opérateur donné
 * @author Axel Serieys
 */
public class Calculatrice {

    private String ope1;
    private String ope2;

    /**
     * Constructeur, initilise les valeurs des deux opérandes
     * Les deux paramètres sont en String car c'est sous ce type qu'elles se trouvent à chaque fois que cette classe est utilisée.
     * À l'avenir, il faudra envisager de prendre en compte des types génériques
     * @param ope1, la valeur de l'opérande de gauche
     * @param ope2, la valeur de l'opérande de droite
     */
    public Calculatrice(String ope1, String ope2) {
        this.ope1 = ope1;
        this.ope2 = ope2;
    }

    /**
     * Réalise le calcul donné avec les valeurs passées en paramètre.
     * @param ope1, l'opérande de gauche
     * @param o, l'opérateur
     * @param ope2, l'opérande de droite
     * @return Resultat, objet contenant la valeur du résultat du calcul, créant une abstraction de son type
     * @throws CompileException, si l'opérateur choisi n'a pas encore été implémenté dans cette classe
     */
    public Resultat calculer(String ope1, String o, String ope2) throws CompileException {
        double temp, temp_ope1, temp_ope2;

        // Si l'opérande est "true", renvoie 1, si c'est "false", renvoie 0, sinon c'est un décimal
        temp_ope1 = (ope1.equals("true") ? 1 : (ope1.equals("false")) ? 0 : Double.parseDouble(ope1));
        temp_ope2 = (ope2.equals("true") ? 1 : (ope2.equals("false")) ? 0 : Double.parseDouble(ope2));

        switch(o) {
            case "+":
                temp = temp_ope1 + temp_ope2;

                return (temp % 1 == 0) ? new Resultat((int) temp) : new Resultat(temp);
            case "-":
                temp = temp_ope1 - temp_ope2;

                return (temp % 1 == 0) ? new Resultat((int) temp) : new Resultat(temp);
            case "*":
                temp = temp_ope1 * temp_ope2;

                return (temp % 1 == 0) ? new Resultat((int) temp) : new Resultat(temp);
            case "/":
                if(temp_ope2 == 0) throw new CompileException("Division par 0 !");
                temp = temp_ope1 / temp_ope2;

                return (temp % 1 == 0) ? new Resultat((int) temp) : new Resultat(temp);
            case "%":
                temp = temp_ope1 % temp_ope2;

                return (temp % 1 == 0) ? new Resultat((int) temp) : new Resultat(temp);
            case "<":
                return new Resultat(Double.parseDouble(ope1) < Double.parseDouble(ope2));
            case "<=":
                return new Resultat(Double.parseDouble(ope1) <= Double.parseDouble(ope2));
            case ">":
                return new Resultat(Double.parseDouble(ope1) > Double.parseDouble(ope2));
            case ">=":
                return new Resultat(Double.parseDouble(ope1) >= Double.parseDouble(ope2));
            case "==":
                return new Resultat(Double.parseDouble(ope1) == Double.parseDouble(ope2));
            default:
                throw new CompileException("OPERATION NON IMPLEMENTÉE : " + o);
        }
    }

    /**
     * Réalise le calcul. Appelle la fonction calculer(String, String, String) en utilisant pour valeur des opérandes
     * les valeurs stockées en attributs de classe
     * @param o, l'opérateur du calcul
     * @return Resultat, objet contenant la valeur du résultat du calcul, créant une abstraction de son type
     * @throws CompileException, si l'opérateur choisi n'a pas encore été implémenté dans cette classe
     */
    public Resultat calculer(String o) throws CompileException {
        return calculer(ope1, o, ope2);
    }
}
