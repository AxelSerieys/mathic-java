package compilateur;

import application.Configuration;
import compilateur.reponse.Reponse;
import exception.*;
import logger.Logger;

import java.util.*;

/**
 * Objet permettant la réalisation de la compilation (traitement)
 * @author Axel Serieys
 *
 */
public class Compilateur {

    private Map<String, Integer> operateurs = new HashMap<>();
    private Logger logger;
    private Configuration config = Configuration.getInstance();

    /**
     * Constructeur de la classe Compilateur.
     * @param logger, instance de la classe Logger, permettant de logger les résultats de la compilation
     */
    public Compilateur(Logger logger) {
        this.logger = logger;

        this.operateurs.put("/", 5);
        this.operateurs.put("*", 5);
        this.operateurs.put("+", 4);
        this.operateurs.put("-", 4);
        this.operateurs.put("(", 0);
        this.operateurs.put(")", 0);
        this.operateurs.put("<", 4);
        this.operateurs.put(">", 4);
        this.operateurs.put("=", 4);
        this.operateurs.put("<=", 4);
        this.operateurs.put(">=", 4);
        this.operateurs.put("==", 4);
        this.operateurs.put("%", 5);
    }
    
    /**
     * Effectue certaines opérations de remplacement dans le calcul donné
     * @param ligne, le calcul
     * @return la ligne modifiée
     * @throws ConfigurationException pour toute erreur liée à la configuration du logiciel
     */
    public String precompilation(String ligne) throws ConfigurationException {
    	// ### ETAPE 1 : Remplacement des constantes
    	if(ligne.contains("%pi%")) {
    		ligne = ligne.replaceAll("%pi%", "" + Constantes.PI());
    	}
    	
    	return ligne;
    }

    /**
     * Effectue la compilation pour les calculs donnés
     * @param calculs, un tableau contenant la liste des calculs à réaliser
     * @return Reponse[], tableau d'objet contenant les éléments de réponse de chaque compilation
     * @throws CompileException pour toute erreur liée à la compilation
     * @throws LoggerException pour toute erreur liée à l'écriture des logs dans le fichier.
     * @throws ConfigurationException pour toute erreur liée à la configuration du logiciel
     */
    public Reponse[] parse(String[] calculs) throws CompileException, LoggerException, ConfigurationException {
    	List<Reponse> reponses = new ArrayList<Reponse>();
    	
    	for(int ligne = 0; ligne < calculs.length; ligne++) {
    		String calcul = calculs[ligne].strip();

    		if(calcul.length() == 0)
    			continue;
    		
    		// Suppression des commentaires
    		if(calcul.contains("#")) {
	    		calcul = calcul.substring(0, calcul.indexOf('#')).strip();
	    		
	    		if(calcul.length() == 0)
	    			continue;
    		}
    		
	        logger.entree(calcul);
			calcul = precompilation(calcul);
	        
	        List<String> array = calcul_as_array(calcul);
	        Reponse reponse = new Reponse();
	        reponse.setCalcul(calcul);
	
	        Queue<String> Q = new LinkedList<>();
	        Stack<String> S = new Stack<>();
	
	        /* ##### 1- Recherche de la RPN ##### */
	        int col = 0;
	        for(String token : array) {
	            col++;
	            if(token.equals("(")) {
	                S.push(token);
	                continue;
	            }
	
	            if(token.equals(")")) {
	                while(!S.peek().equals("(")) {
	                    Q.add(S.pop());
	                }
	                S.pop();
	                continue;
	            }

	            // Si c'est un opérateur
	            if(operateurs.containsKey(token)) {
	                while (!S.empty() && operateurs.get(token) <= operateurs.get(S.peek()))
	                    Q.add(S.pop());
	                S.push(token);
	                continue;
	            }

				if(isNumber(token)) {
					Q.add(token);
					continue;
				}
	
	            throw new CompileException("Entrée invalide : '" + token + "'", ligne, col);
	        }
	
	        while (!S.isEmpty())
	            Q.add(S.pop());
	
	        String[] tabRPN = new String[Q.size()];
	        Q.toArray(tabRPN);
	        logger.RPN(tabRPN);
	        List<String> RPN = new ArrayList<String>(Arrays.asList(tabRPN));
	        String RPNstr = listToString(RPN);
	        reponse.setRPN(RPNstr);
			Resultat resultat;
	
	        /* ##### 2- Calcul du résultat ##### */
	        while(RPN.size() > 2) {
	            int colonne = 0;
	            if (operateur_exists(RPN)) {
	                for(int i = 0; i < RPN.size(); i++) {
	                    colonne++;
	                    if(this.operateurs.containsKey(RPN.get(i))) {
							String str_ope1 = RPN.remove(i-2);
							String str_ope2 = RPN.remove(i-2);
	                        String operation = RPN.get(i-2);

							resultat = new Calculatrice(str_ope1, str_ope2).calculer(operation);
							RPN.set(i-2, resultat.getValeur().toString());
	                        break;
	                    }
	                }
	            } else {
	                throw new CompileException("Aucun opérateur restant dans la pile !", ligne);
	            }
	        }
	
	        // S'il ne reste plus qu'un seul élément dans la liste, c'est le résultat attendu
	        if(RPN.size() == 1) {
	            logger.resultat(RPN.get(0), true);
	            reponse.setResultat(Resultat.fromString(RPN.get(0)));
	            reponses.add(reponse);
	        } else if(RPN.get(1).equals("-") && Character.isDigit(RPN.get(0).charAt(0))) {
	        	String res = RPN.get(1) + RPN.get(0);
				logger.resultat(res, true);
				reponse.setResultat(Resultat.fromString(res));
				reponses.add(reponse);

			} else {
	            throw new CompileException("Il manque une opérande dans la file !", ligne);
	        }
    	}
    	
    	return reponses.toArray(new Reponse[0]);
    }
    
    /**
     * Fonction outil permettant de convertir une liste de String en une string
     * @param List<String> list 
     * @return String
     */
    private String listToString(List<String> list) {
    	String ret = "";
    	for(String str : list)
    		ret += str + " ";
    	return ret;
    }

    /**
     * Fonction outil permettant de trouver l'index d'une sous-chaine de caractères dans une autre
     * @param str, la chaîne dans laquelle on cherche
     * @param toSearch, la sous-chaîne à chercher
     * @return l'index trouvé, -1 si elle n'est pas présente
     */
    private int StringIndexOf(String str, String toSearch) {
        char[] arr = str.toCharArray();
        for(int i = 0; i < arr.length; i++)
            if(String.valueOf(arr[i]).equals(toSearch))
                return i;
        return -1;
    }

    /**
     * Détermine s'il reste un opérateur dans la pile (+, -, <, >, ==, % ...)
     * @param List<String> list, la liste des éléments algébriques restants
     * @return true si il reste au moins un opérateur, false sinon
     */
    private boolean operateur_exists(List<String> list) {
        for(String s : list)
            if(operateurs.containsKey(s))
                return true;
        return false;
    }

    /**
     * Permet de déterminer si une String est numérique (double)
     * @param String str, la chaîne en question
     * @return true si elle correspond à un double, false sinon
     */
    private boolean isNumber(String str) {
        try {
            Double.valueOf(str);
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    /**
     * Convertit un calcul sous forme de chaîne de caractères en un tableau. Chaque case cotient soit un nombre, soit un opérateur.
     * @param calcul (String), le calcul à convertir
     * @return String[] le tableau contenant tous les éléments du calcul
     */
    private List<String> calcul_as_array(String calcul) {
        List<String> array = new ArrayList<>(Collections.nCopies(calcul.length(), ""));
        int index = 0;

		for(char c : calcul.toCharArray()) {
            if(c == '\n')
                continue;

            // Tous les caractères qui peuvent séparer des nombres
            if(operateurs.containsKey(c+"")) {
                if(array.get(index).length() > 0)
                    index++;
                else if(c == '=' && index > 0 && (array.get(index-1).equals("=") || array.get(index-1).equals(">") || array.get(index-1).equals("<")))
                    index--;
                array.set(index, array.get(index)+c);

                if(c != '-' || (index != 0 && isNumber(array.get(index-1))))
                	index++;
            } else if(c == ' ') {
                index++;
            } else if(c == '=' && index > 0 && (array.get(index - 1).equals("<") || array.get(index-1).equals(">"))) {
                    array.set(index - 1, array.get(index - 1) + "=");
            } else {
                array.set(index, array.get(index)+c);
            }
        }

        array.removeIf(String::isEmpty); // Suppression des cases vides du tableau
        return array;
    }
}
