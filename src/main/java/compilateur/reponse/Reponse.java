package compilateur.reponse;

import compilateur.Resultat;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import application.Popup;

/**
 * Objet Reponse, correspond au résultat d'une compilation
 * @author Axel Serieys
 *
 */
public class Reponse {
	
	private String calcul;
	private String RPN;
	private Resultat resultat;
	
	/**
	 * Constructeur de la classe Reponse
	 * @param calcul, le calcul demandé
	 * @param RPN, le même calcul en notation polonaise inversée
	 * @param resultat, le résultat du calcul
	 */
	public Reponse(String calcul, String RPN, Resultat resultat) {
		this.calcul = calcul;
		this.RPN = RPN;
		this.resultat = resultat;
	}

	public Reponse() {}

	/**
	 * Setter du champ calcul
	 * @param calcul, le calcul donné
	 */
	public void setCalcul(String calcul) {
		this.calcul = calcul;
	}
	
	/**
	 * Setter du champ RPN (notation polonaise inversée)
	 * @param RPN, le calcul en notation polonaise inversée
	 */
	public void setRPN(String RPN) {
		this.RPN = RPN;
	}
	
	/**
	 * Setter du champ resultat
	 * @param resultat, le résultat du calcul
	 */
	public void setResultat(Resultat resultat) {
		this.resultat = resultat;
	}

	/**
	 * Getter du champ calcul
	 * @return String
	 */
	public String getCalcul() {
		return this.calcul;
	}
	
	/**
	 * Getter du champ RPN (notation polonaise inversée)
	 * @return String
	 */
	public String getRPN() {
		return this.RPN;
	}
	
	/**
	 * Getter du champ résultat
	 * @return Resultat, instance de la classe Resultat
	 */
	public Resultat getResultat() {
		return this.resultat;
	}
	
	@Override
	/**
	 * Affiche l'objet Reponse sous forme de String.
	 */
	public String toString() {
		return ""+resultat.getValeur();
	}
	
	/**
	 * Permet de transformer un tableau de Reponse en un String
	 * @param reponses, le tableau contenant les réponses
	 * @return String
	 */
	public static String arrayToString(Reponse[] reponses) {
		if(reponses.length == 1) {
			return reponses[0].toString();
		} else {
			String str = "";
			for(Reponse reponse : reponses) {
				str += reponse.resultat.getValeur() + "\n";
			}

			return str.substring(0, str.length()-1);
		}
	}
	
	/**
	 * Permet de convertir une String au format JSON en un tableau de Reponses (1 ou plus)
	 * @param jsonObject, la String qui contient les réponses au format JSON 
	 * @return Reponse[]
	 */
	public static Reponse[] fromJSON(String jsonObject) {
		Reponse[] reponses = null;
		try {
			JSONArray ja = new JSONObject(jsonObject).getJSONArray("calculs");
			reponses = new Reponse[ja.length()];
			
			for(int i = 0; i < ja.length(); i++) {
				JSONObject obj = ja.getJSONObject(i);
				String calcul = obj.getString("calcul");
				String RPN = obj.getString("RPN");
				String resultat = String.valueOf(obj.getFloat("resultat"));

				try {
					int val = Integer.parseInt(resultat);
					reponses[i] = new Reponse(calcul, RPN, new Resultat(val));
				} catch(NumberFormatException e) {
					double val = Double.parseDouble(resultat);
					reponses[i] = new Reponse(calcul, RPN, new Resultat(val));
				}
			}
			
		} catch(JSONException e) {
			new Popup(Popup.T_ERREUR, "Réponse non conforme", "La réponse envoyée par le serveur n'est pas conforme.\nLe serveur et le client exécutent-t-ils la même version ?"
					+ "\nRéponse : " + jsonObject
					+ "\n\n" + e.getMessage()
			).show();
		}
		
		return reponses;
	}
	
	/**
	 * Permet de convertir un tableau de Reponse en une String au format JSON
	 * @param reponses, un tableau de réponses
	 * @return String, au format JSON
	 */
	public static String toJSON(Reponse[] reponses) {
		String ret = "{\n\tcalculs: [";
		
		for(Reponse reponse : reponses) {
			ret += "\n\t\t{calcul: \"" + reponse.getCalcul() + "\", RPN: \"" + reponse.getRPN() + "\", resultat: \"" + reponse.getResultat().getValeur() + "\"},";
		}
		ret = ret.substring(0, ret.length()-1);
		ret += "\n\t]\n}";
		
		return ret;
	}
}
