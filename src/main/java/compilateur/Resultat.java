package compilateur;

import application.Configuration;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

/**
 * Contient le résultat d'un calcul
 * Répond à la problématique suivante : un résultat peut avoir différents types (entier, double, booléen...)
 * Cette classe sert donc à contenir un résultat dont le type n'est pas statique
 * @author Axel Serieys
 */
public class Resultat<T extends Object> {

    private final T valeur;
    private final Class type;
    private static Configuration config = Configuration.getInstance();
    private static final DecimalFormat formatter = new DecimalFormat("#.####", DecimalFormatSymbols.getInstance(Locale.ENGLISH));

    /**
     * Constructeur de la classe Resultat, permet d'enregistrer une valeur d'un type héritant de java.lang.Object
     * @param valeur, la valeur du résultat à stocker
     */
    public Resultat(T valeur) {
        formatter.setRoundingMode(RoundingMode.DOWN);
        formatter.setMinimumFractionDigits(0);
        formatter.setMaximumFractionDigits(config.getInt("compilateur.precision", 3));

        if(valeur instanceof Double) {
            this.valeur = (T) new Double(formatter.format(valeur));
        } else {
            this.valeur = valeur;
        }
        this.type = valeur.getClass();
    }

    /**
     * Retourne la valeur stockée
     * @return T, la valeur du résultat
     */
    public T getValeur() {
        return this.valeur;
    }

    /**
     * @deprecated
     * Retourne le type de la valeur choisi.
     * Deprecated : Éviter d'utiliser cette méthode autant que possible, car c'est une mauvaise pratique
     * @return Class, la classe du type de la valeur stockée (héritant de java.lang.Object)
     */
    public Class getType() {
        return this.type;
    }

    /**
     * Convertit un résultat sous forme de String en objet Resultat ayant pour valeur, le resultat dans un
     * type adapté. Le paramètre résultat est donc converti en booléen, entier ou réel.
     * @param resultat, le resultat sous forme de String
     * @return Resultat, une instance de la classe Resultat
     */
    public static Resultat fromString(String resultat) {
        if (resultat.equals("true") || resultat.equals("false")) {
            boolean val = resultat.equals("true");
            return new Resultat(val);
        }

        try {
            int val = Integer.parseInt(resultat);
            return new Resultat(val);
        } catch(NumberFormatException e) {
            double val = Double.parseDouble(resultat);
            return new Resultat(val);
        }
    }
}
