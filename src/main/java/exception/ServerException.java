package exception;

/**
 * Gestion des exceptions liées au serveur mathic
 * @author Axel Serieys
 *
 */
public class ServerException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur de l'exception
	 * @param message, le message d'erreur
	 */
	public ServerException(String message) {
		super(message);
	}
}
