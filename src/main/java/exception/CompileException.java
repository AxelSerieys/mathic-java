package exception;

/**
 * Gestion des exceptions liées à la compilation (traitement effectif du compilateur)
 * @author Axel Serieys
 *
 */
public class CompileException extends Exception {

	private static final long serialVersionUID = 1L;
	private Integer ligne, colonne;

	/**
	 * Constructeur de la classe.
	 * @param erreur, le message d'erreur
	 */
    public CompileException(String erreur) {
        super(erreur);
    }

    /**
     * Constructeur de la classe contenant le numéro de ligne où l'erreur s'est produite
     * @param erreur, le message d'erreur
     * @param ligne, le numéro de la ligne où l'erreur s'est produite
     */
    public CompileException(String erreur, int ligne) {
        super(erreur);
        this.ligne = ligne;
    }

    /**
     * Constructeur de la classe contenant le numéro de ligne et la colonne où l'erreur s'est produite
     * @param erreur, le message d'erreur
     * @param ligne, le numéro de la ligne où l'erreur s'est produite
     * @param colonne, le numéro de la colonne où l'erreur s'est produite
     */
    public CompileException(String erreur, int ligne, int colonne) {
        super(erreur);
        this.ligne = ligne;
        this.colonne = colonne;
    }

    /**
     * Permet de retourner la ligne où l'erreur s'est produite
     * @return Integer, la ligne où l'erreur s'est produite
     */
    public Integer getLine() {
        return this.ligne;
    }

    /**
     * Permet de retourner la colonne où l'erreur s'est produite
     * @return Integer, la colonne où l'erreur s'est produite
     */
    public Integer getColonne() {
        return this.colonne;
    }

}
