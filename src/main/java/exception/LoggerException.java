package exception;

/**
 * Gestion des exceptions liées aux logs de l'application
 * @author Axel Serieys
 *
 */
public class LoggerException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Constructeur de l'exception
	 * @param erreur, le message d'erreur
	 * @param fileName, le nom du fichier de logs dont la gestion a lancé une exception
	 */
	public LoggerException(String erreur, String fileName) {
        super(erreur + " (" + fileName + ")");
    }
}
