package exception;

/**
 * Gestion des exceptions liées à la configuration de l'application
 * @author Axel Serieys
 *
 */
public class ConfigurationException extends Exception {
	
	private static final long serialVersionUID = 1L;
	int ligne;

	/**
	 * Constructeur de l'exception
	 * @param message, le message d'erreur
	 */
	public ConfigurationException(String message) {
		super(message);
		this.ligne = -1;
	}
	
	/**
	 * Constructeur de l'exception contenant le numéro de la ligne où l'erreur s'est produite
	 * @param message, le message d'erreur
	 * @param ligne, la ligne où l'erreur s'est produite
	 */
	public ConfigurationException(String message, int ligne) {
		super(message);
		this.ligne = ligne;
	}

	@Override
	public String getMessage() {
		if(ligne == -1)
			return super.getMessage();
		else
			return super.getMessage() + " (ligne " + ligne + ")";
	}
}
