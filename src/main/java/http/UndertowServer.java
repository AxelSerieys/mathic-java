package http;

import org.json.JSONArray;
import org.json.JSONObject;

import io.undertow.Undertow;
import application.Configuration;
import application.Popup;
import compilateur.Compilateur;
import compilateur.reponse.Reponse;
import exception.CompileException;
import exception.ConfigurationException;
import exception.LoggerException;
import exception.ServerException;
import logger.Logger;
import io.undertow.server.HttpHandler;
import io.undertow.server.HttpServerExchange;
import io.undertow.util.Headers;
import io.undertow.util.HttpString;

/**
 * Classe UndertowServer : création et gestion du serveur HTTP
 * @author Axel Serieys
 *
 */
public class UndertowServer implements HttpHandler {
	
	private String address;
	private int port;
	private Undertow server;
	private boolean loop;

	private Logger logger;
	private Configuration conf;
	private Compilateur compilateur;

	/**
	 * Constructeur de la classe
	 * @param host, le nom d'hôte du serveur et son port au format {hote}:{port}
	 */
	public UndertowServer(String host) {
		conf = Configuration.getInstance();
		this.address = host.split(":")[0];
		this.port = Integer.parseInt(host.split(":")[1]);
		
		try {
	        this.logger = new Logger(conf.get("logs.fichier_serveur"));
		} catch (LoggerException | ConfigurationException e) {
			new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
		}
		
		compilateur = new Compilateur(this.logger);
		
		this.server = Undertow.builder()
				.addHttpListener(this.port, this.address)
				.setHandler(this).build();
	}
	
	/**
	 * Démarre le serveur
	 */
	public void start() {
		this.server.start();
		this.loop = true;
		
		while(loop) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				new Popup(Popup.T_ERREUR, "InterruptedException", e.getMessage()).show();
			}
		}
	}
	
	/**
	 * Arrête le serveur
	 */
	public void stop() {
		this.loop = false;
	}

	@Override
	/**
	 * Cette fonction est appelée automatiquement lorsqu'une requête est reçue par le serveur
	 * HttpServerExchange objet contenant le contexte, géré par Undertow.
	 * @throws SeverException si une erreur se produit du côté du serveur
	 * @throws ConfigurationException si une erreur est présente dans la configuration de l'application
	 * @throws LoggerException si une erreur se produit lors de la gestion des logs
	 * @throws CompileException si une erreur liée à la compilation se produit
	 */
	public void handleRequest(HttpServerExchange exchange) throws ServerException, ConfigurationException, LoggerException, CompileException {
		StringBuilder sb = new StringBuilder();
		exchange.getRequestReceiver().receiveFullBytes((e, m) -> {
			sb.append(new String(m));
	    });
		String postData = sb.toString();

		// vérification de la validité de la requête
		if(postData.length() == 0) {
			throw new ServerException("Aucun paramètre POST fourni");
		}
		
		if(!postData.contains("calcul:")) {
			throw new ServerException("Requête mal formulée, aucune variable calcul fournie");
		}
		
		// remplacement des caractères modifiés par le moteur de recherche
		postData = postData.replace("%2B", "+");
		postData = postData.replace("%2F", "/");
		
		// récupération du calcul
		JSONArray calculs = new JSONObject(postData).getJSONArray("calculs");
		String[] calculsTab = new String[calculs.length()];
		
		for(int i = 0; i < calculs.length(); i++) {
			JSONObject element = calculs.getJSONObject(i);
			String calcul = element.getString("calcul");
			calculsTab[i] = calcul;
			
			System.out.println("Input: " + calcul);
		}	
		
		Reponse[] reponses = compilateur.parse(calculsTab);
		
		// réalisation du calcul et envoi de la réponse au client
		exchange.getResponseHeaders().put(Headers.CONTENT_TYPE, "application/json");
		exchange.getResponseHeaders().put(new HttpString("Access-Control-Allow-Origin"), "*");
		exchange.getResponseSender().send(Reponse.toJSON(reponses));
	}
}
