package application;

import java.io.IOException;
import java.util.Scanner;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import application.controller.PopupController;

/**
 * Permet d'afficher une popup contenant un message et un titre
 * @author Axel Serieys
 *
 */
public class Popup {
	
	private static final int WINDOW_WIDTH = 600;
	private static final int WINDOW_HEIGHT = 200;
	
	private static final String IC_ERREUR = "icones/error.png";
	private static final String IC_WARNING = "icones/warning.png";
	
	public static final int T_ERREUR = 0x1;
	public static final int T_CHOIX = 0x2;
	
	public static final int CHOIX_1 = 0x10;
	public static final int CHOIX_2 = 0x11;

	private Stage stage;
	private String titre;
	private String message;
	private int choix = -1;
	private int type = -1;
	
	/**
	 * Constructeur de Popup
	 * @param type, le type de popup : T_ERREUR ou T_CHOIX
	 * @param titre, le titre du message
	 * @param message, le message à afficher
	 */
	public Popup(int type, String titre, String message) {
		this.message = message;
		this.titre = titre;
		this.type = type;
	}
	
	/**
	 * Permet de fermer la fenêtre Popup
	 */
	public void fermerPopup() {
		stage.close();
	}
	
	/**
	 * Permet de mettre à jour le choix effectué par l'utilisateur (mis à jour dans le controller)
	 * @param choix
	 */
	public void setChoix(int choix) {
		this.choix = choix;
	}
	
	/**
	 * Permet d'obtenir le choix effectué par l'utilisateur
	 * @return int
	 */
	public int getChoix() {
		return this.choix;
	}
	
	/**
	 * Affiche la popup. Si le Thread courant est Application ou event, on affiche la fenêtre popup.
	 * Sinon, on affiche le message demandé dans la console.
	 * Si le type de la Popup est T_CHOIX, on entre en mode bloquant en attendant la réponse de l'utilisateur.
	 * @return int, le choix effectué. Correspond à la valeur de CHOIX_1 ou CHOIX_2
	 */
	public int show() {
		String threadName = Thread.currentThread().getName();
		if(threadName.equals("JavaFX Application Thread") || threadName.equals("event")) {
			stage = new Stage();
			BorderPane root;
			FXMLLoader fxmlLoader;
			try {
				fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("vues/Popup.fxml"));
				root = (BorderPane) fxmlLoader.load();
				Scene scene = new Scene(root, WINDOW_WIDTH, WINDOW_HEIGHT);	
				scene.getStylesheets().add(getClass().getClassLoader().getResource("application.css").toExternalForm());
	 
				PopupController popupController = fxmlLoader.<PopupController>getController();
				popupController.initialiser(this, this.titre, this.message, type);
				
				switch(type) {
					case T_ERREUR:
						popupController.setImage(IC_ERREUR);
						break;
					case T_CHOIX:
						popupController.setImage(IC_WARNING);
						break;
					default:
						new Popup(T_ERREUR, "Erreur", "Le type de la popup n'a pas été spécifié").show();
						break;
				}
				
				stage.setScene(scene);
				stage.setResizable(false);
				stage.initModality(Modality.APPLICATION_MODAL);
				stage.setTitle("Mathic");
				
				if(type == T_ERREUR) {
					stage.show();
				} else if(type == T_CHOIX){ // On attend que l'utilisateur ait fait son choix
					stage.showAndWait();
				}
				
				return this.choix;
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("##### Erreur qui a provoqué l'exception : #####");
				System.out.println(this.titre);
				System.out.println(this.message);
				System.out.println("#     #####     #");
				
				return -1;
			}
		} else {
			String msg = "[" + threadName + "] " + this.message;
			System.out.print(msg);
			
			if(type == T_CHOIX) {
				System.out.println(" (O/n)");
			
				return (((String) new Scanner(System.in).nextLine()).toUpperCase().subSequence(0, 1).equals("O") ? Popup.CHOIX_1 : Popup.CHOIX_2);
			} else {
				System.out.println();
				return Popup.CHOIX_1;
			}
		}
	}
}
