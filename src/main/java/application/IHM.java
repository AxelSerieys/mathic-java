package application;

import java.io.IOException;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;

/**
 * La classe Application de la fenêtre principale de l'IHM.
 * @author Axel Serieys
 *
 */
public class IHM extends Application {
	
	private static final int WIDTH = 500;
	private static final int HEIGHT = 800;
	
	@Override
	/**
	 * Ouverture de la fenêtre principale de l'IHM.
	 */
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getClassLoader().getResource("vues/main.fxml"));
			Scene scene = new Scene(root, WIDTH, HEIGHT);
			scene.getStylesheets().add(getClass().getClassLoader().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setResizable(false);
			primaryStage.setTitle("Mathic");
			primaryStage.show();
		} catch(IOException e) {
			new Popup(Popup.T_ERREUR, "IOException", e.getMessage()).show();
		}
	}
	
	/**
	 * Permet de démarrer le lancement de la fenêtre sur le Thread Application de JavaFX
	 * @param args
	 */
	public static void main(String args[]) {
		launch(args);
	}
}
