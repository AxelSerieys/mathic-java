package application;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import compilateur.Constantes;
import exception.ConfigurationException;

/**
 * Permet de gérer la configuration de l'application grâce au fichier mathic.cfg
 * @author Axel Serieys
 *
 */
public class Configuration {

	private final File configFile = new File("mathic.cfg");
	private Map<String, String> elements = new HashMap<String, String>();
	private static Configuration configurationObject = null;
	public static boolean QUIET_MODE = false;
	
	/**
	 * Constucteur de la classe. On demande à créer le fichier s'il n'existe pas et on appelle lireFichierConfiguration()
	 * pour lire le fichier
	 * Ce constructeur est privé. Pour créer un objet, il faut appeler newInstance()
	 */
	private Configuration() {
		if(!configFile.exists())
			creerConfigFile();

		lireFichierConfiguration();
	}
	
	/**
	 * Permet de créer un nouvel objet de type Configuration
	 * L'objet est stocké et peut être récupéré grâce à un appel à newInstance()
	 * @return objet Configuration
	 */
	public static Configuration newInstance() {
		Configuration.configurationObject = new Configuration();
		return configurationObject;
	}
	
	/**
	 * Permet de retourner l'objet Configuration stocké.
	 * Retourne null si aucun appel à newInstance() n'a été réalisé.
	 * @return objet Configuration
	 */
	public static Configuration getInstance() {
		return Configuration.configurationObject;
	}
	
	/**
	 * Demande à l'utilisateur s'il souhaite créer automatiquement le fichier de configuration
	 * Récupère sa réponse. Si oui, le fichier est créé (appel à creerConfigDefaut())
	 * Sinon, l'application est fermée.
	 */
	public void creerConfigFile() {
		int choix = new Popup(Popup.T_CHOIX, "Fichier de configuration introuvable", "Fichier de configuration introuvable. Voulez-vous le créer ?").show();

		if(choix == Popup.CHOIX_1) {
			creerConfigDefaut();
		} else if(choix == Popup.CHOIX_2) {
			System.exit(0);
		} else { // la fenêtre a été fermée
			System.exit(1);
		}
	}
	
	/**
	 * Permet de créer le fichier de configuration.
	 */
	private void creerConfigDefaut() {
		String configDefaut = "# fichier de configuration Mathic\n\n";
		configDefaut += "[COMPILATEUR]\n";
		configDefaut += "precision=4\n\n";
		configDefaut += "[SERVEUR]\n";
		configDefaut += "adresse=localhost\n";
		configDefaut += "port=12345\n\n";
		configDefaut += "[LOGS]\n";
		configDefaut += "repertoire=logs/\n";
		configDefaut += "fichier_client=mathic.log\n";
		configDefaut += "fichier_serveur=serveur_mathic.log";
		
		try {
			configFile.createNewFile();
			FileWriter fw = new FileWriter(configFile);
			
			fw.write(configDefaut);
			fw.close();
		} catch (IOException e) {
			new Popup(Popup.T_ERREUR, "IOException", e.getMessage()).show();
		}
	}
	
	/**
	 * Lit le fichier de configuration et rempli la HashMap elements avec les éléments du fichier
	 */
	public void lireFichierConfiguration() {
		FileReader fr;
		BufferedReader br;
		try {
			fr = new FileReader(configFile);
			br = new BufferedReader(fr);
			String ligne;
			int ligneC = 0;
			String titre = "";
			
			while((ligne = br.readLine()) != null) {
				ligneC++;
				// Suppression des commentaires
	    		if(ligne.contains("#")) {
		    		ligne = ligne.substring(0, ligne.indexOf('#')).strip();
	    		}
	    		if(ligne.length() == 0)
	    			continue;
	    		
	    		ligne = ligne.strip().toLowerCase();
	    		
	    		if(ligne.startsWith("[") && ligne.endsWith("]")) {
	    			titre = ligne.replace("[", "").replace("]", "");
	    			continue;
	    		}
	    		
	    		if(ligne.contains("=")) {
	    			String[] elem = ligne.split("=");
	    			elements.put(titre+"."+elem[0], elem[1]);
	    		} else {
	    			throw new ConfigurationException("Élément de configuration invalide : " + ligne, ligneC);
	    		}
			}
			fr.close();
		} catch (IOException e) {
			new Popup(Popup.T_ERREUR, "Exception", e.getMessage()).show();
			System.exit(Constantes.ERREUR_IOEXCEPTION);
		} catch(ConfigurationException e) {
			new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
			System.exit(Constantes.ERREUR_CONFIGURATION_INVALIDE);
		}
	}
	
	/**
	 * Permet de retourner la valeur d'un élément de configuration grâce à sa clé unique sous forme de String
	 * @param key, le nom de l'élément de configuration
	 * @return String, la valeur de l'élément de configuration
	 * @throws ConfigurationException si l'élément n'existe pas
	 */
	public String get(String key) throws ConfigurationException {
		if(elements.containsKey(key)) {
			return elements.get(key);
		} else {
			throw new ConfigurationException("Élément de configuration introuvable : " + key);
		}
	}
	
	/**
	 * Permet de retourner la valeur d'un élément de configuration grâce à sa clé unique sous forme de int
	 * Si la clé n'existe pas, ConfigurationException est lancée
	 * @param key, le nom de l'élément de configuration
	 * @return int, la valeur de l'élément de configuration
	 * @throws ConfigurationException si l'élément n'existe pas
	 */
	public int getInt(String key) throws ConfigurationException {
		return Integer.valueOf(this.get(key));
	}

	/**
	 * Permet de retourner la valeur d'un élément de configuration grâce à sa clé unique sous forme de int
	 * Si la clé n'existe pas, le paramètre defaultValue est retourné et aucune exception n'est lancée
	 * @param key, le nom de l'élément de configuration
	 * @param defaultValue, la valeur par défault, retournée si l'élément key n'existe pas en configuration
	 * @return int, la valeur de l'élément de configuration
	 */
	public int getInt(String key, int defaultValue) {
		try {
			return Integer.valueOf(this.get(key));
		} catch(ConfigurationException e) {
			return defaultValue;
		}
	}
}
