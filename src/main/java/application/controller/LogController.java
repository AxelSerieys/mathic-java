package application.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Configuration;
import exception.ConfigurationException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import application.Popup;

/**
 * Contrôleur de la fenêtre d'affichage des logs. Permet de gérer les interractions avec ses éléments
 * @author Axel Serieys
 *
 */
public class LogController implements Initializable {

	@FXML
	private TextArea taLogs;
	
	@Override
	/**
	 * Permet d'initialiser le contrôleur de la fenêtre.
	 * Cette méthode est appelée automatiquement lors de la création de la fenêtre
	 * On utilise la fonction de JavaFX Platform.runLater(), qui permet d'exécuter tout son contenu dans le
	 * Thread Application.
	 */
	public void initialize(URL location, ResourceBundle resources) {
		Configuration config = Configuration.getInstance();

		Platform.runLater(() -> {
			try {
				File fichier = new File(new File(config.get("logs.repertoire")), config.get("logs.fichier_client")); // "logs/mathic.log"
				if(fichier != null && fichier.exists()) {
					FileReader fr = new FileReader(fichier);
					BufferedReader br = new BufferedReader(fr);
					String ligne, content = "";
					
					while((ligne = br.readLine()) != null) {
						content += ligne.strip() + '\n';
					}
					fr.close();
					
					taLogs.setText(content.substring(0, content.length()-1));
					taLogs.requestFocus();
					taLogs.positionCaret(taLogs.getLength());
				} else {
					((Stage) taLogs.getScene().getWindow()).close();
					String message;
					if(Configuration.QUIET_MODE)
						message = "L'outil a été exécuté avec l'option -q qui supprime le logging";
					else
						message = "Le fichier de logs n'existe pas.\nIl devrait se trouver dans le répertoire logs/ et appelé 'mathic.log'";

					new Popup(Popup.T_ERREUR, "Fichier de logs introuvable", message).show();
				}
			} catch(IOException | ConfigurationException e) {
				new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
			}
		});
	}
}
