package application.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import javafx.scene.paint.Color;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Paint;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import application.Configuration;
import application.Popup;
import compilateur.Compilateur;
import compilateur.reponse.Reponse;
import exception.CompileException;
import exception.ConfigurationException;
import exception.LoggerException;
import logger.Logger;

import java.io.InputStreamReader;

/**
 * Contrôleur de la fenêtre principale de l'IHM. Permet de gérer les interractions avec ses éléments
 * @author Axel Serieys
 *
 */
public class ClientController implements Initializable {
	
	@FXML
	private TextField tfHote, tfPort;
	@FXML
	private TextArea taResult, taCalcul, taLogs;
	@FXML
	private ChoiceBox<String> cbMethode;
	@FXML
	private Label lbErreur, lbHote, lbPort, lbMethode, lbVersion;
	@FXML
	private RadioButton rbLocal, rbDistant;
	@FXML
	private ImageView ivUploadFile, ivSaveFile;
	
	private Logger logger;
	private Compilateur compilateur;
	private Configuration conf;
	
	private static final Paint COULEUR_ROUGE = Color.web("#DE5833");
	private static final Paint COULEUR_VERTE = Color.web("#66AD57");
	private static final int DELAI_DISP_MESSAGES = 5000;

	@Override
	/**
	 * Permet d'initialiser le contrôleur de l'IHM.
	 * Cette méthode est appelée automatiquement lors de la création de la fenêtre
	 */
	public void initialize(URL location, ResourceBundle resources) {
		conf = Configuration.getInstance();
		try {
			logger = new Logger(conf.get("logs.fichier_client"));
			compilateur = new Compilateur(logger);
		} catch(LoggerException | ConfigurationException e) {
        	new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
		}
		
		ContextMenu menu = new ContextMenu();
		
		MenuItem logs = new MenuItem("Voir les logs");
		menu.getItems().add(logs);
		
		menu.setOnAction(event -> {
			MenuItem item = (MenuItem) event.getTarget();
			
			if(item == logs) {
				onClickLogs();
			} else {
				afficherErreur("Interaction inconnue...");
			}
		});
		
		taResult.setContextMenu(menu);
		
		// Ajout des tooltip aux deux images-boutons
		Tooltip.install(ivUploadFile, new Tooltip("Importer"));
		Tooltip.install(ivSaveFile, new Tooltip("Enregistrer"));
	}
	
	/**
	 * Permet d'afficher une erreur en une ligne sur l'IHM
	 * Le message est affiché en rouge
	 * @param message, le message de l'erreur
	 */
	private void afficherErreur(String message) {
		lbErreur.setTextFill(COULEUR_ROUGE);
		lbErreur.setText(message);
		lbErreur.setVisible(true);
	}
	
	/**
	 * Permet d'afficher un message de succès en une ligne sur l'IHM
	 * Le message est affiché en vert et disparaît au bout de $DELAI_DISP_MESSAGES ms (5000 par défaut).
	 * @param message, le message à afficher
	 */
	private void afficherSucces(String message) {
		lbErreur.setTextFill(COULEUR_VERTE);
		lbErreur.setText(message);
		lbErreur.setVisible(true);
		
		new Timer().schedule(new TimerTask() {
			
			@Override
			public void run() {
				lbErreur.setVisible(false);
			}
		}, DELAI_DISP_MESSAGES);
	}
	
	/**
	 * Cette méthode est appelée lorsque l'on clique sur "Afficher les logs" sur le menu
	 * qui est affiché en cliquant droit sur la TextArea du bas (résultats de la compilation)
	 * Elle permet d'afficher le contenu du fichier de logs de mathic qui se trouve par défaut
	 * dans ./logs/mathic.log
	 */
	private void onClickLogs() {
		// Afficher les logs
		Stage stage = new Stage();
		BorderPane root;
		try {
			root = FXMLLoader.load(getClass().getClassLoader().getResource("vues/logView.fxml"));
			Scene scene = new Scene(root, 450, 600);
			scene.getStylesheets().add(getClass().getClassLoader().getResource("application.css").toExternalForm());
			
			stage.setScene(scene);
			stage.setResizable(true);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
		} catch (IOException e) {
			new Popup(Popup.T_ERREUR, "Erreur fatale", e.getMessage()).show();
		}
	}
	
	@FXML
	/**
	 * Cette méthode est appelée lorsque le bouton "Compiler" est cliquée.
	 * Elle sert à vérifier la validité des informations entrées et à lancer la compilation
	 * @param event ActionEvent
	 */
	private void onClick(ActionEvent event) {
		lbErreur.setVisible(false);
		if(taCalcul.getText().length() == 0) {
			afficherErreur("Erreur : aucun calcul spécifié");
			return;
		}

		String[] lignes = taCalcul.getText().toString().split(System.getProperty("line.separator"));

		if(rbLocal.isSelected()) {
			try {
				String res = "";
	            Reponse[] reponses = compilateur.parse(lignes);
				
				for(int i = 0; i < reponses.length; i++) {
					res += "Calcul : " + reponses[i].getCalcul();
					res += "\n    RPN : " + reponses[i].getRPN();
					res += "\n    Resultat : " + reponses[i].getResultat().getValeur() + " (" + reponses[i].getResultat().getType().getSimpleName() + ")";
					res += "\n";
				}
		        taResult.setText(res.substring(0, res.length()-1));
			} catch(LoggerException | CompileException | ConfigurationException e) {
				try {
					logger.erreur(e.getMessage());
				} catch (LoggerException f) {
					afficherErreur(f.getMessage());
				}
				afficherErreur("Erreur : " + e.getMessage());
			}
		} else {
			if (tfHote.getText().length() == 0) {
				afficherErreur("Erreur : aucun hôte spécifié");
				return;
			} else if (tfPort.getText().length() == 0) {
				afficherErreur("Erreur : aucun port spécifié");
				return;
			}
			
			String methode = cbMethode.getValue();
			String calcul = taCalcul.getText();
			String hote = tfHote.getText();
			int port;
			
			try {
				port = Integer.parseInt(tfPort.getText());
			} catch(NumberFormatException e) {
				afficherErreur("Erreur : port invalide");
				return;
			}
			
			if(!hote.startsWith("http")) {
				hote = "http://" + hote;
			}
			
			URL url;
			HttpURLConnection connexion;
			try {
				url = new URL(hote + ":" + port);
				connexion = (HttpURLConnection) url.openConnection();
			} catch (IOException e) {
				afficherErreur(e.getMessage());
				return;
			}
			
			try {
				connexion.setRequestMethod(methode);
				connexion.setRequestProperty("User-Agent", "Mathic-UI");
				connexion.setRequestProperty("Content-Type", "application/json");
				
				// Création de la sortie en JSON
				String sortie = "{\n\tcalculs: [\n";
				for(int i = 0; i < lignes.length; i++) {
					sortie += "\t\t{calcul: \"" + lignes[i] + "\", ligne: " + (i+1) + "},\n";
				}
				sortie = sortie.substring(0, sortie.length()-2);
				sortie += "\n\t]\n}";
				
				// Ajout des données
				connexion.setDoOutput(true);
				OutputStream os = connexion.getOutputStream();
				OutputStreamWriter writer = new OutputStreamWriter(os, "UTF-8");
				writer.write(sortie);
				writer.flush();
				writer.close();
				os.close();
				
				Integer responseCode = connexion.getResponseCode();
				String toPrint = "Code de retour : " + responseCode + " " + connexion.getResponseMessage();
				logger.http(responseCode, connexion.getResponseMessage());
				if(responseCode == 200) {
					BufferedReader br = new BufferedReader(new InputStreamReader(connexion.getInputStream()));
					
					toPrint += "\n\nRéponse du serveur :\n";
					// Utiliser un objet Response !! (le traduire en français)
					String reponseJSON = "";
					String ligne;
					if(br != null) {
						while((ligne = br.readLine()) != null)
							reponseJSON += ligne;
					}
					
					Reponse[] reponses = Reponse.fromJSON(reponseJSON);

					for(int i = 0; i < reponses.length; i++) {
						toPrint += "Calcul : " + reponses[i].getCalcul();
						toPrint += "\n    RPN : " + reponses[i].getRPN();
						toPrint += "\n    Resultat : " + reponses[i].getResultat();
						toPrint += "\n";

						logger.calcul(reponses[i].getCalcul());
						logger.resultat(reponses[i].getResultat(), false);
					}
				}
				logger.fin();
		        taResult.setText(toPrint);
			} catch (ProtocolException e) {
				afficherErreur("Erreur : Protocole inconnu");
			} catch (LoggerException e) {
				new Popup(Popup.T_ERREUR, e.getClass().getCanonicalName(), e.getMessage()).show();
			} catch (IOException e) {
				afficherErreur(e.getMessage());
			}
		}
	}
	
	@FXML
	/**
	 * Cette fonction est appelée automatiquement lorsque l'utilisateur clique sur le bouton "Importer fichier"
	 * Elle permet de choisir un fichier, le lire, et afficher son contenu dans la TextArea "taCalcul" 
	 * @param event, ActionEvent
	 */
	private void onUploadFile(MouseEvent event) {
		try {
			File fichier = ouvrirExplorateurDeFichiersImport(new File("input/"));
			if(fichier != null) {
				FileReader fr = new FileReader(fichier);
				BufferedReader br = new BufferedReader(fr);
				String ligne, content = "";
				
				while((ligne = br.readLine()) != null) {
					content += ligne + '\n';
				}
				fr.close();
				
				taCalcul.setText(content.substring(0, content.length()-1));
				taCalcul.requestFocus();
				taCalcul.positionCaret(taCalcul.getLength());
			}
		} catch(IOException e) {
			afficherErreur(e.getMessage());
		}
	}
	
	@FXML
	/**
	 * Cette fonction est appelée automatiquement lorsque l'utilisateur clique sur le bouton "Enregistrer"
	 * Elle permet de choisir un fichier, puis d'écrire dedans le contenu de la TextArea "taCalcul"
	 * @param event
	 */
	private void onSaveFile(MouseEvent event) {
		try {
			File fichier = ouvrirExplorateurDeFichiersExport(new File("input/"));
			if(fichier != null) {
				fichier.createNewFile();
				FileWriter fw = new FileWriter(fichier);
				
				fw.write(taCalcul.getText());
				afficherSucces("Contenu enregistré [" + fichier.getName() + "]");
				fw.close();
			}
		} catch(IOException e) {
			afficherErreur(e.getMessage());
		}
	}
	
	/**
	 * Permet d'ouvrir l'explorateur de fichiers du système pour enregistrer un fichier
	 * @param dossier, le dossier par défaut qui sera ouvert. S'il n'existe pas, le dossier dans lequel se trouve l'exécutable sera ouvert.
	 * @return File, le fichier choisi (Attention, peut valoir null)
	 */
	private File ouvrirExplorateurDeFichiersExport(File dossier) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Sauvegarder...");
		
		if(dossier.exists())
			fileChooser.setInitialDirectory(dossier);
        
		fileChooser.getExtensionFilters().addAll(
			new FileChooser.ExtensionFilter(".src", "*.src"),
            new FileChooser.ExtensionFilter("Tous les fichiers", "*.*")
		);
		
		return fileChooser.showSaveDialog(null);
	}
	
	/**
	 * Permet d'ouvrir l'explorateur de fichiers du système pour importer un fichier
	 * @param dossier, le dossier par défaut qui sera ouvert. S'il n'existe pas, le dossier dans lequel se trouve l'exécutable sera ouvert.
	 * @return File, le fichier choisi (Attention, peut valoir null)
	 */
	private File ouvrirExplorateurDeFichiersImport(File dossier) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Importer...");
		
		if(dossier.exists())
			fileChooser.setInitialDirectory(dossier);
		
		fileChooser.getExtensionFilters().addAll(
			new FileChooser.ExtensionFilter(".src", "*.src"),
            new FileChooser.ExtensionFilter("Tous les fichiers", "*.*")
		);
	    
		return fileChooser.showOpenDialog(null);
	}

	@FXML
	/**
	 * Cette fonction est appelée lorsque l'on clique sur le RadioButton "Serveur local".
	 * Elle permet de cacher les zones de saisies relatives à l'appel à un serveur
	 * telles que l'hote du serveur, son port et la méthode utilisée
	 * @param event, ActionEvent
	 */
	private void onServeurLocal(ActionEvent event) {
		lbHote.setVisible(false);
		lbPort.setVisible(false);
		lbMethode.setVisible(false);
		cbMethode.setVisible(false);
		tfHote.setVisible(false);
		tfPort.setVisible(false);
	}

	@FXML
	/**
	 * Cette fonction est appelée lorsque l'on clique sur le RadioButton "Serveur distant".
	 * Elle permet d'afficher les zones de saisies relatives à l'appel à un serveur
	 * telles que l'hote du serveur, son port et la méthode utilisée
	 * @param event
	 */
	private void onServeurDistant(ActionEvent event) {
		lbHote.setVisible(true);
		lbPort.setVisible(true);
		lbMethode.setVisible(true);
		cbMethode.setVisible(true);
		tfHote.setVisible(true);
		tfPort.setVisible(true);
	}
}
