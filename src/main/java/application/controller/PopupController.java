package application.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import application.Popup;

/**
 * Contrôleur de la fenêtre Popup. Permet de gérer les interractions avec ses éléments
 * @author Axel Serieys
 *
 */
public class PopupController {

		@FXML
		private TextArea taMessage;
		@FXML
		private Label lbTitre;
		@FXML
		private Button bt1, bt2;
		@FXML
		private ImageView ivIcone;
		
		private int type;
		private Popup popupObject;
		
		/**
		 * Permet d'initialiser le contrôleur. Cette méthode <b>DOIT</b> être appelée avant l'ouverture de la fenêtre.
		 * @param popupObject, l'instance de la classe Popup qui l'appelle
		 * @param titre, le titre du message
		 * @param message, le message de la popup
		 * @param type, le type de message. Soit Popup.T_ERREUR, soit Popup.T_CHOIX
		 */
		public void initialiser(Popup popupObject, String titre, String message, int type) {
			this.lbTitre.setText(titre);
			this.taMessage.setText(message);
			this.popupObject = popupObject;
			this.type = type;
			
			switch(type) {
				case Popup.T_ERREUR:
					bt1.setVisible(false);
					bt2.setText("OK");
					break;
				case Popup.T_CHOIX:
					bt1.setVisible(true);
					bt1.setText("OUI");
					bt2.setText("NON");
					break;
				default:
					break;
			}
		}
		
		/**
		 * Permet de mettre à jour l'image qui est affichée sur la popup
		 * @param path, le chemin vers l'image
		 */
		public void setImage(String path) {
			ivIcone.setImage(new Image(path));
		}
		
		@FXML
		/**
		 * Cette fonction est appelée quand le bouton 1 est cliqué
		 * C'est à dire quand la Popup est de type T_CHOIX et que le bouton cliqué est "OUI"
		 * @param event, ActionEvent
		 */
		private void onBt1(ActionEvent event) {
			switch(this.type) {
				case Popup.T_CHOIX:
					popupObject.setChoix(Popup.CHOIX_1);
					popupObject.fermerPopup();
					break;
			}
		}
		
		@FXML
		/**
		 * Cette fonction est appelée quand le bouton 2 est cliqué
		 * C'est à dire quand la Popup est de type T_CHOIX et que le bouton cliqué est "NON"
		 * Ou quand la Popup est de type T_ERREUR et que le bouton cliqué est "OK"
		 * @param event
		 */
		private void onBt2(ActionEvent event) {
			switch(this.type) {
				case Popup.T_ERREUR:
					popupObject.setChoix(Popup.CHOIX_1);
					popupObject.fermerPopup();
					break;
				case Popup.T_CHOIX:
					popupObject.setChoix(Popup.CHOIX_2);
					popupObject.fermerPopup();
					break;
			}
		}
}
