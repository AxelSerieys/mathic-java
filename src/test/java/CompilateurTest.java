import application.Configuration;
import compilateur.reponse.Reponse;
import exception.CompileException;
import exception.ConfigurationException;
import exception.LoggerException;
import logger.Logger;
import org.junit.Test;

import compilateur.Compilateur;

import static org.junit.Assert.*;

/**
 * Classe de test du compilateur
 * @author Axel Serieys
 */
public class CompilateurTest {

    private Compilateur compilateur;
    private static int TEST_ID = 1; // incrément du test. Le constructeur est appelé pour chaque test, il est incrémenté à chaque fois

    /**
     * Constructeur du test
     */
    public CompilateurTest() {
        Configuration.newInstance();
        // Si la variable n'existe pas, on la place à false par défaut
        Configuration.QUIET_MODE = System.getProperty("quiet.mode") != null && System.getProperty("quiet.mode").equals("true");

        try {
            Logger logger = new Logger("test." + TEST_ID + ".log");
            this.compilateur = new Compilateur(logger);
        } catch(LoggerException e) {
            System.out.println("Le fichier de log n'a pas pu être créé : tests.log");
        }

        TEST_ID++;
    }

    /**
     * Exécute un ensemble de requêtes passées en paramètre par le compilateur mathic
     * @param calcul, chaque case du tableau contient une ligne à exécuter
     * @return Reponse[], le tableau des réponses
     */
    private Reponse[] compile(String[] calcul) {
        try {
            return this.compilateur.parse(calcul);
        } catch (CompileException | LoggerException | ConfigurationException e) {
            fail(e.getMessage());
        }
        return null;
    }

    /**
     * Exécute un ensemble de requêtes passées en paramètre par le compilateur mathic
     * Les exceptions ne sont pas inhibées (utile lorsque l'on s'attend à en reçevoir une)
     * @param calcul, chaque case du tableau contient une ligne à exécuter
     * @return Reponse[], le tableau des réponses
     * @throws CompileException
     * @throws LoggerException
     * @throws ConfigurationException
     */
    private Reponse[] compileAllowExceptions(String[] calcul) throws CompileException, LoggerException, ConfigurationException {
        return this.compilateur.parse(calcul);
    }

    /**
     * Exécute la requête passée en paramètre par le compilateur mathic
     * @param calcul, le calcul à exécuter
     * @return String, le résultat du calcul (et pas une instance de Resultat)
     */
    private Object compile(String calcul) {
        return this.compile(new String[]{calcul})[0].getResultat().getValeur();
    }

    /**
     * Permet de tester l'addition sous différents cas
     */
    @Test
    public void addition() {
        assertEquals(4, compile("2+2"));
        assertEquals(4, compile("2 + 2"));
        assertEquals(7, compile("1.5 + 5.5"));
        assertEquals(6.32, compile("1.52 + 4.8"));
        assertEquals(0, compile("-2+2"));
        assertEquals(3, compile("-2+(2+3)"));
        assertEquals(-7, compile("-(3+4)"));
    }

    /**
     * Permet de tester la soustraction sous différents cas
     */
    @Test
    public void soustraction() {
        assertEquals(1, compile("2-1"));
        assertEquals(-1, compile("2 - 3"));
        assertEquals(-4, compile("1.5 - 5.5"));
        assertEquals(-3.28, compile("1.52 - 4.8"));
        assertEquals(-5, compile("-2-3"));
        assertEquals(0, compile("2-2"));
        assertEquals(9, compile("10-(3-2)"));
    }

    /**
     * Permet de tester la multiplication sous différents cas
     */
    @Test
    public void multiplication() {
        assertEquals(4, compile("2*2"));
        assertEquals(-4, compile("-2*2"));
        assertEquals(-4, compile("2*(-2)"));
        assertEquals(0, compile("2*(2-2)"));
        assertEquals(2, compile("2*(-3+4)"));
        assertEquals(16.491, compile("1.76*9.37"));
    }

    /**
     * Permet de tester la division sous différents cas
     */
    @Test
    public void division() {
        assertEquals(1, compile("2/2"));
        assertEquals(-1, compile("-2/2"));
        assertEquals(-1, compile("2/(-2)"));
        assertEquals(8, compile("2/(1/4)"));
        assertThrows(CompileException.class, () -> {
            compileAllowExceptions(new String[] {"2/0"});
        });
    }

    /**
     * Permet de tester les comparaisons sous différents cas
     */
    @Test
    public void comparaisons() {
        assertEquals(true, compile("2>1"));
        assertEquals(true, compile("2>=1"));
        assertEquals(false, compile("5<4"));
        assertEquals(false, compile("5<=4"));
        assertEquals(true, compile("3<=4"));
        assertEquals(true, compile("4<=4"));
        assertEquals(true, compile("4>=4"));
        assertEquals(true, compile("7==7"));
        assertEquals(false, compile("2==3"));
        assertEquals(false, compile("-2 < -3"));
        assertEquals(false, compile("-2 <= -3"));
        assertEquals(false, compile("2 <= -3"));
    }

    /**
     * Permet de vérifier la conversion d'un booléen obtenu par une condition
     * en un entier capable d'être membre d'un calcul
     * faux = 0
     * vrai = 1
     */
    @Test
    public void maths_comparaisons() {
        assertEquals(1, compile("1 + (1 > 2)"));
        assertEquals(2, compile("1 + (1 < 2)"));
    }


    /**
     * Permet de tester les commentaires sous différents cas
     */
    @Test
    public void commentaires() {
        assertEquals(3, compile(new String[] {"# ceci est un commentaire", "1+2"})[0].getResultat().getValeur());
        assertEquals(3, compile(new String[] {"1+2 # ceci est un commentaire"})[0].getResultat().getValeur());
    }

    /**
     * Permet de tester les modulo sous différents cas
     */
    @Test
    public void modulo() {
        assertEquals(0, compile("2%2"));
        assertEquals(0, compile("4%2"));
        assertEquals(-1, compile("-3%2"));
        assertEquals(2, compile("2%4"));
        assertEquals(2, compile("2%-4"));
    }

    /**
     * Permet de tester les constantes sous différents cas
     */
    @Test
    public void constantes() {
        assertEquals(3.142, compile("%pi%"));
        assertEquals(9.426, compile("3*%pi%"));
        assertEquals(6.284, compile("%pi%+%pi%"));
        assertEquals(-3.142, compile("-%pi%"));
    }

}